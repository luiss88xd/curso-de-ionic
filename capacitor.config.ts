import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'car2',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
