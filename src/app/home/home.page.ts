import { Component } from '@angular/core';
import { FormGroup,FormBuilder,Validators,FormControl } from '@angular/forms';
import { Database } from './../providers/database';
import { AlertController, ToastController } from '@ionic/angular';
import { IonModal } from '@ionic/angular';
import { UpdateComponent } from './../update/update.component';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

	data:any = [];
	record:any = [];
	isModalOpen:boolean = false;

	data_child:any;

	nombre:string = "Luis";
	apellido:string = "Solares";
	homeForm:FormGroup;
	validate_messages:any = {
		nombre:[
			{
				type:'required', message: 'El campo Nombre es requerido',
			}
		],

		apellido:[
			{
				type:'required', message: 'El campo Apellido es requerido'
			}
		],

		edad:[
			{
				type:'required', message: 'El campo Edad es requerido'
			}
		]
	}

	constructor(
		private formBuilder:FormBuilder,
		private database: Database,
		private alert: AlertController,
		private toast: ToastController
	){

		this.homeForm = this.formBuilder.group({
			nombre: new FormControl(
				"",
				Validators.compose([
					Validators.required
				])
			),

			apellido: new FormControl(
				"",
				Validators.compose([
					Validators.required
				])
			),

			edad: new FormControl(
				"",
				Validators.compose([
					Validators.required
				])
			)
		});

		this.list();
	}

	list(){
		this.database.getData().then((res:any)=>{
			this.data = res;
			this.record = res;
		})
	}

	search(data:any){
		var search:string = data.target.value.toLowerCase();

		this.data = this.record.filter( (x:any) => {
			var data = (x.nombre + x.apellido + x.edad).toLowerCase();
			if(data.includes(search))
				return x;
		})

	}

	validar(data:any){
		this.database.saveData(data).then((res:any)=>{
			this.list();
		})
	}

	async presentAlert(data:any) {
	    const alert = await this.alert.create({
	      header: 'Advertencia ¿Está seguro de eliminar este registro?',
	      buttons: [
	        {
	          text: 'Cancelar',
	          role: 'cancel',
	          handler: () => {
	            console.log("se canceló");
	          },
	        },
	        {
	          text: 'OK',
	          role: 'confirm',
	          handler: () => {
	            this.database.deleteData(data);
	            this.list();
	            this.presentToast('El registro ha sido eliminado correctamente');
	          },
	        },
	      ],
	    });

	    await alert.present();

	    const { role } = await alert.onDidDismiss();
	}

	delete(data:any){
		this.presentAlert(data);
		//this.database.deleteData(data);
	}

	async presentToast(message:string) {
		const toast = await this.toast.create({
		  message: message,
		  duration: 3000,
		  position: 'bottom'
		});

		await toast.present();
	}

	cancel(){
		this.isModalOpen = false;
	}

	confirm(){
		this.isModalOpen = false;
		this.list();
	}

	update(data:any,position:number){
		this.isModalOpen = true;
		data.id = position;
		this.data_child = data;
	}

}
