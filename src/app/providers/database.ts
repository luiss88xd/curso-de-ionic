import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage-angular';

@Injectable()
export class Database{
	
	constructor(
		private storage:Storage
	){
		this.storage.create();
	}

	saveData(data:any){

		return new Promise((resolve,reject)=>{
			this.getData().then((res:any)=>{

				var record = [];
				for(let x of res){
					record.push(x);
				}

				record.push(data);
				this.storage.set('data',JSON.stringify(record)).then((savedData:any)=>{
					resolve(savedData);
				})
			})
		})
	}

	getData(){
		return new Promise((resolve,reject)=>{
			this.storage.get('data').then((res:any)=>{

				if(res){
					const json = JSON.parse(res);
					resolve(json);	
				}else{
					resolve([]);
				}
			})
		})
	}

	deleteData(data:any){
		this.getData().then((res:any)=>{
			for(var x = 0; x < res.length;x++){
				if(
					res[x].nombre == data.nombre &&
					res[x].apellido == data.apellido &&
					res[x].edad == data.edad
				){
					res.splice(x,1);
				}
			}

			this.storage.set('data',JSON.stringify(res));
		})
	}

	updateData(data:any,position:number){
		return new Promise((resolve,reject)=>{
			this.getData().then((res:any)=>{
				res[position].nombre = data.nombre;
				res[position].apellido = data.apellido;
				res[position].edad = data.edad;

				this.storage.set('data',JSON.stringify(res)).then((x:any)=>{
					resolve(true);
				});

			});
		});
	}
}


