import { Component, OnInit, Input } from '@angular/core';
import { FormGroup,FormBuilder,Validators,FormControl } from '@angular/forms';
import { Database } from './../providers/database';

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.scss'],
})
export class UpdateComponent implements OnInit {

	@Input('data') data:any;
	form!:FormGroup;

	constructor(
		private formBuilder: FormBuilder,
		private database: Database
	){}

	ngOnInit() {
		this.setInputs();
	}

	setInputs(){
		this.form = this.formBuilder.group({
			nombre: new FormControl(
				this.data.nombre,
				Validators.compose([
					Validators.required
				])
			),

			apellido: new FormControl(
				this.data.apellido,
				Validators.compose([
					Validators.required
				])
			),

			edad: new FormControl(
				this.data.edad,
				Validators.compose([
					Validators.required
				])
			)
		})
	}

	update(record:any){
		this.database.updateData(record,this.data.id).then((res)=>{
			
		});
	}

}
